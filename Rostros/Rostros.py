import cv2
import sys

rostroCascade = cv2.CascadeClassifier('C:/opencv/sources/data/haarcascades/haarcascade_frontalface_default.xml')

imagen = cv2.imread('fcbarcelona.jpg') 
filtro = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY) 

rostros = rostroCascade.detectMultiScale(
	filtro,
	scaleFactor = 1.2,
	minNeighbors = 5,
	minSize= (30,30),
	flags = cv2.CASCADE_SCALE_IMAGE
)

for (x, y, w, h) in rostros:
	cv2.rectangle(imagen, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow("Rostros encontrados", imagen) 
cv2.waitKey(0) 